
" Sources each vim file in a directory (not recursive, for now).
function loadSources#SourceDirectory (dir_name)
    let file_list = split(globpath(expand(a:dir_name), '*.vim'), '\n')
    for fpath in file_list
        execute 'source' fpath
    endfor
endfunction

function loadSources#Source_Vim_Includes ()
    call loadSources#SourceDirectory("~/.vim/vimrc.d")
endfunction
