
" Helper to reload the current instance's vim configuration. This is a very
" handy function to have.
function helpers#ReloadVimrc()
    source ~/.vim/vimrc
    echo "Reloaded Vimrc"
endfunction

" Works like GNU Linux's 'touch' built-in, but makes directories instead.
function helpers#TouchDirectory(path)
    if !isdirectory(expand(a:path))
      call mkdir(expand(a:path))
    endif
endfunction


" Implements a dummy 'full_screen' mode, only works in GUI... obviously.
function helpers#ToggleFullScreen()

    " This function should do nothing when there is no GUI.
    if !has("gui") | return | endif

    if &guioptions =~# 'C'
        set guioptions-=C
        if exists('s:go_temp')
            if s:go_temp =~# 'm'
                set guioptions+=m
            endif
            if s:go_temp =~# 'T'
                set guioptions+=T
            endif
        endif
        if has('win32') || has('win64')
            simalt ~r
        endif

        let [&lines, &columns] = [s:lines_save, s:columns_save]
    else
        let s:go_temp = &guioptions
        set guioptions+=C
        set guioptions-=m
        set guioptions-=T
        if has('win32') || has('win64')
            simalt ~x
        endif

        let [s:lines_save, s:columns_save] = [&lines, &columns]

        set columns=999
        set lines=999
    endif
endfunction


" Closes a buffer (default bind is <C-c>) intelligently. It closes window
" buffers where appropriate and only deletes the window buffer if it is the
" last buffer in said window.
function helpers#SmartCloseBuffer()
    let fn_buffer_in_window = "winbufnr(v:val) == bufnr('%')"
    let win_num = winnr('$')
    let bufs_in_window = filter(range(1, win_num), fn_buffer_in_window)

    " If there are more than one buffers in the current window, we should just
    " delete this buffer.
    let should_delete_buf = len(bufs_in_window) > 1
    let is_nerdtree = matchstr(expand("%"), 'NERD') == 'NERD'

    " If appropriate, delete the buffer. Otherwise, close the window.
    if !should_delete_buf | wincmd c
    elseif is_nerdtree | wincmd c
    else | bdelete | endif
endfunction
