function! s:AddFiletypeMatch(fmatch, ftype)
    " NOTE: `:setf` is the shorthand equivalent to `:setfiletype`
    execute 'autocmd BufRead,BufNewFile '. a:fmatch .'  setf '. a:ftype
endfunction

augroup filetypedetect
    call s:AddFiletypeMatch('*.service',     'systemd')
    call s:AddFiletypeMatch('*.target',      'systemd')
    call s:AddFiletypeMatch('*.socket',      'systemd')
    call s:AddFiletypeMatch('*.slice',       'systemd')
    call s:AddFiletypeMatch('*.tfstate',     'json')
    call s:AddFiletypeMatch('*.tfstate.backup', 'json')
    call s:AddFiletypeMatch('*.rs',          'rust')
    call s:AddFiletypeMatch('*.vimp',        'vimperator')
    call s:AddFiletypeMatch('*bash-*',       'sh.bash')
    call s:AddFiletypeMatch('*mutt-*',       'mutt')
    call s:AddFiletypeMatch('.vimperatorrc', 'vimperator')
    call s:AddFiletypeMatch('README.md',     'markdown.README')
    call s:AddFiletypeMatch('README.mkd',    'markdown.README')
augroup END
