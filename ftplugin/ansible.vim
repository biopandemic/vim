function! DefaultAnsibleFile()
python << EOF
import vim
blen = len(vim.current.buffer)
if blen == 0:
    print "empty ansible."
EOF
endfunction

augroup filetype_ansible
    autocmd!
    autocmd filetype ansible vnoremap <buffer> <leader>: :'<,'>s/=/: /g<cr>
augroup end
