" Automatically save when focus is lost.
setlocal autowrite

" Disable folding by default.
setlocal foldmethod=indent nofoldenable
