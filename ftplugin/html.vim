let b:start = 'xdg-open %'
nmap <buffer><silent> <leader>r :Start!<cr>
autocmd BufWritePre *.html :%s/\s\+$//e

setl expandtab
setl shiftwidth=2
setl softtabstop=2

" Disable color column, line wrap...
setl colorcolumn=0
setl nowrap
