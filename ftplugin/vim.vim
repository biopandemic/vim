" runonce
if exists("b:did_ftplugin")
    finish
endif
let b:did_ftplugin = 1

autocmd BufWritePost <buffer> silent! source ~/.vim/vimrc<cr>:ft detect<cr>

" Default `marker` folds are completely closed.
setlocal foldenable
setlocal foldmethod=marker
setlocal foldlevel=0

" Documentation Key bind.
nmap <buffer><silent> K :copen help <cword><cr>
vmap <buffer><silent><expr> K :copen help <cword><cr>

" Build keybind: Source & reload file.
nmap <buffer><silent> <leader>b :source %<cr>:filetype detect<cr>
nmap <buffer><silent> <leader>r :w<cr>:source %<cr>:e<cr>:filetype detect<cr>
