let b:dispatch = "docker build -f % -t " . expand("%") . " ."

cmap <buffer> DockerBuild Make! build

" augroup ft_Dockerfile
"     au!
"     au BufRead      <buffer> :DockerBuild
"     au BufWritePost <buffer> :DockerBuild
" augroup END

" vi: ft=vim
