" less.vim: LESS CSSS Buffer-Level Config

" runonce
if exists("b:did_ftplugin")
    finish
endif
let b:did_ftplugin = 1

" LessToCSS Auto Compile on save.
function! LessToCss()
    let current_file = shellescape(expand('%:p'))
    let filename = shellescape(expand('%:r'))
    let command = "silent !lessc " . current_file . " " . filename . ".css"
    Dispatch! command
endfunction

autocmd BufWritePost,FileWritePost <buffer> call LessToCss()
