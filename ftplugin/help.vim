setl hidden
setl nomodifiable

" Auto close all help messages when focus lost.
" autocmd BufLeave,FocusLost * if &ft ==# "help" | silent! bdelete! | endif
