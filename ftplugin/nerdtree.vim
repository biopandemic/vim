nmap  <buffer> gb execute 'tabedit ' . g:NERDTreeBookmarksFile . '<cr>'

augroup ftplugin_nerdtree
    autocmd BufLeave,FocusLost <buffer> silent! NERDTreeClose
augroup end
