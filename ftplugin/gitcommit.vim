finish

if bufname("%") ==# 'COMMIT_EDITMSG'
    augroup ftplugin_gitcommit
        autocmd BufReadPost <buffer> call setpos('.', [0, 1, 1, 0])

        " Automatically start in insert mode.
        autocmd BufEnter,FocusGained <buffer> startinsert!

        " Automatically destroy git commit buffers.
        " autocmd BufLeave,FocusLost   <buffer> bdelete!
    augroup end
endif

