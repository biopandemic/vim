
" Automatically destroy git commit buffers.
" autocmd BufLeave,FocusLost * if &ft ==# 'tagbar' | bdelete! | endif
