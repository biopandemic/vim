" Local Python Vim Settings (many stolen from PyMode.vim)
let b:dispatch = 'python %'

setlocal complete+=t

setlocal formatoptions-=t
setlocal formatoptions+=w

if v:version > 702 && !&relativenumber
    setlocal number
endif

" Disable auto-wrapping lines.
setlocal nowrap

setlocal wrapscan

" Python enforces that files be >80 Columns wide.
setlocal textwidth=78

setlocal commentstring=#%s

setlocal define=^\s*\\(def\\\\|class\\)

" Use syntax folding in python.
setl foldmethod=syntax
setl foldlevel=1

" Automatically PEP8 all files before write.
" autocmd BufWritePre PymodeLintAuto

" Build Keybind
nmap <buffer> <leader>b :Dispatch python <cfile><cr>

" Documentation Key bind.
nmap <buffer><silent> K :Pydoc <cword><cr>
