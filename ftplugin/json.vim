setl nofoldenable
setl expandtab
setl shiftwidth=2
setl softtabstop=2

au BufReadPost <buffer> exe jdaddy#reformat('jdaddy#outer_pos', v:count1)
