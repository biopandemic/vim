" automatically strip whitespace.
autocmd BufWritePre *.js  :%s/\s\+$//e
autocmd BufWritePre *.jsx :%s/\s\+$//e

" set whitespace for javascript to always use 2 spaces instead of tabs
setlocal expandtab
setlocal shiftround
setlocal shiftwidth=2
setlocal softtabstop=2

syntax region foldBraces start=/{/ end=/}/ transparent fold keepend extend
setlocal nofoldenable
setlocal foldmethod=syntax
setlocal foldlevel=2
