# Jake's Vim Configs

Pretty awesome, I think. Should be pretty well commented... But it changes
often so honestly... No promises. Probably not the best example of my code
organization skills!

## Install
```
# Clone the repo, but make sure to do it recursively.
git clone --recursive git@github.com:JakeLogemann/vim.git ~/.vim

# Copy/Link/Move Vimrc to ~/.vimrc
ln -s ~/.vim/vimrc ~/.vimrc

# Allow NeoBundle to install plugins.
vim

# Build the auto-completion (should now be done via NeoBundle).
cd ~/.vim/bundle/YouCompleteMe && ./install.sh

```
