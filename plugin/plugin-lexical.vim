" REFERENCE
" =========
" Commands
"
" Vim offers many standard key mappings for spell-checking and completion.
" Spell-check
"
" These are the Normal mode commands:
"
"     ]s - Move to next misspelled word after the cursor.
"     [s - Like ]s but search backwards
"     ]S - Like ]s but only stop at bad words, not at rare words or words for another region.
"     [S - Like ]S but search backwards.
"
" With the following key mappings you can use Visual mode selection to select the characters (including whitespace). Otherwise the word under the cursor is used.
"
"     zg - Mark as a good word
"     zw - Like zg but mark the word as a wrong (bad) word.
"     zug - Unmark as good word
"     zuw - Unmark as wrong (bad) word
"
"     z= - For the word under/after the cursor suggest correctly spelled words
"     1z= - Use the first suggestion, without prompting
"
"     . - Redo - repeat last word replacement
"
"     :spellr - Repeat the replacement done by z= for all matches with the replaced word in the current window
"
" For spelling suggestions while in Insert mode:
"
"     «CTRL-X» «CTRL-S» (or «CTRL-X» «s» for terminal users) - suggest spelling, using «CTRL-P» and «CTRL-N» to navigate.
let g:lexical#spell = 0         " 0=disabled, 1=enabled

let g:lexical#dictionary_key = '<leader>k'
let g:lexical#spell_key = '<leader>s'
let g:lexical#thesaurus_key = '<leader>t'

let g:lexical#thesaurus = [
      \ expand('~/.vim/spell/thesaurus-mthesaur.txt'),
      \ ]

let g:lexical#dictionary = [
      \ '/usr/share/dict/words',
      \ '~/.vim/spell/en.utf-8.add',
      \ ]

augroup lexical
  autocmd!
  autocmd FileType markdown,mkd call lexical#init()
  autocmd FileType textile call lexical#init()
  autocmd FileType text call lexical#init({ 'spell': 0 })
augroup END

