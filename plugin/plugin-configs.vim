"          Language-Specific Plugin Configurations
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
augroup filetype_go
    au!
    au BufWritePre *.go :GoImports
    au BufWritePre *.go :GoFmt
    au BufWritePost *.go :Make!
    au FileType go nnoremap <buffer> K GoDoc<cr>
augroup END

"                Misc Plugin Configurations
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Do not show buffer numbers.
let g:bufferline_modified = '+'
let g:bufferline_echo = 0
let g:bufferline_active_buffer_left = ''
let g:bufferline_active_buffer_right = ''

let g:bufferline_show_bufnr = 0

" scroll buffers automatically
let g:bufferline_rotate = 2

let g:session_autoload = 'no'
let g:session_autosave = 'yes'
let g:session_autosave_periodic = 3 "minutes

silent execute '!mkdir -p "' . expand("~/.cache/vim/sessions") . '"'
let g:session_directory = expand("~/.cache/vim/sessions")

let g:ackprg = 'ag --vimgrep'
let g:ycm_key_invoke_completion = '<C-b>'

cabbrev gitv Gitv
cabbrev gs Gstatus
cabbrev gc Gcommit
cabbrev gb Gblame
cabbrev gps Git push
cabbrev gpl Git pull


"                VimWiki Configurations
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
cabbrev wiki VimwikiIndex
cabbrev daily VimwikiDiaryIndex

nnoremap <leader><leader>i   :VimwikiIndex<cr>
nnoremap <leader><leader>d   :VimwikiDiaryIndex<cr>
