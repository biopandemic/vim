"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                                 Keybinds
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Toggle insert mode. This probably won't work in a terminal
" version of vim, but thats probably fine.
" inoremap <S-Space> <ESC>
" nnoremap <S-Space> i

" Space is a great leader key.
let mapleader=" "

" Quick Tab Keybinds
nnoremap <silent> <leader><Tab>   :tabNext<cr>
nnoremap <silent> <leader><S-Tab> :tabPrev<cr>

" Quick Switch Windows
nnoremap <silent> <leader>h <c-w>h
nnoremap <silent> <leader>j <c-w>j
nnoremap <silent> <leader>k <c-w>k
nnoremap <silent> <leader>l <c-w>l

nnoremap <silent> <left>    <c-w>h
nnoremap <silent> <right>   <c-w>l
nnoremap <silent> <up>      <c-w>k
nnoremap <silent> <down>    <c-w>j
vnoremap <silent> <leader>= :Tabularize multiple_spaces<cr>

" Sidebars / Quick Windows {{{1
nnoremap <silent> <leader>e :NERDTreeToggle<cr>
nnoremap <silent> <leader>t :TagbarToggle<cr>
nnoremap <silent> <leader>g :Gstatus<cr>

" Alias "Visual Block" mode to something more easy to reach.
nnoremap <silent> <leader>v <c-v>

" Vim Basics
" ----------

" Remove the 'Ex Mode' keybind, since there's two and it annoys me.
nmap Q <Nop>

" Yank to End :: Make Y act like C & D.
nmap Y y$

" Goto file (under cursor).
nmap <silent> gF :view <cfile><cr>

" Goto buffer
nnoremap <silent> gb :Unite -resume buffer<cr>
nnoremap <silent> gB :Unite -resume bookmark<cr>


" Toggle Keys
" -----------
nnoremap <silent> ]r :set noreadonly<cr>
nnoremap <silent> ]# :set norelativenumber<cr>
nnoremap <silent> ]m :set nomodifiable<cr>
nnoremap <silent> ]w :set nowrap<cr>

nnoremap <silent> [r :set readonly<cr>
nnoremap <silent> [# :set relativenumber<cr>
nnoremap <silent> [m :set modifiable<cr>
nnoremap <silent> [w :set wrap<cr>


" GUI Management Keybinds {{{1
" Toggle full screen mode (should just do nothing in terminal).
nnoremap <silent> <F11> :call helpers#ToggleFullScreen()<CR>

" Close buffers intelligently with Ctrl-C.
nnoremap <silent> <C-c> :call helpers#SmartCloseBuffer()<cr>



" {{{ Normal Mode Keybinds
" F5 trims all whitespace from teh file.
nmap <silent> <F5> :let _s=@/<Bar>:%s/\s\+$//e<Bar>:let @/=_s<Bar>:nohl<CR>


" Yank to End :: Make Y act like C & D.
nmap Y y$

" Backspace Toggle Highlighting
nmap <BS> :set hlsearch! hlsearch?<cr>


" Remap F1 to spell checker.
nmap <silent> <F1> :set spell! spell?<cr>

" Remap F3 to toggle Syntastic.
nmap <silent> <F3> :SyntasticToggleMode<cr>

" }}}
" {{{ Insert Mode Keybinds

" }}}
" {{{ Leader Keybinds


" Tab Shortcuts
nmap <leader>t      <nop>
nmap <leader>tn     :tabnew<CR>
nmap <leader>tc     :tabclose<CR>

" Manage .vimrc easier.
nnoremap <silent> <leader>v :NERDTreeToggle ~/.vim<cr>
nnoremap <silent> <leader>V :source ~/.vim/vimrc<cr>:filetype detect<cr>

" Bind `,s` to sort a selection.
vmap <silent> <leader>s :sort<cr>

" View Marks with `,m`
nmap <silent> <leader>m :marks<cr>

" Tab a definitionlist quickly with `,dl`
vmap <silent> <leader>dl :Tabularize /=<cr>


" <leader>s :: Session management
nmap <silent> <leader>se    :UltiSnipsEdit<cr>
nmap <silent> <leader>so    :OpenSession<cr>
nmap <silent> <leader>ss    :SaveSession
nmap <silent> <leader>sd    :DeleteSession<cr>
nmap <silent> <leader>sc    :CloseSession<cr>

" Visual Mode
" -----------

" Move selections of text up and down with arrow keys.
vnoremap <up> ddkkp
vnoremap <down> ddp

vnoremap <C-J>     dp
vnoremap <C-K>     dkkp


" Unite Keybinds
" --------------
nnoremap <silent> <C-p> :Unite -toggle -buffer-name=git file_rec/git<cr>

" Control + Left/Right indents text.
vnoremap <C-H>     <
vnoremap <C-L>     >

" }}}



" Make C-c work as clipboard.
vmap <silent><C-c> "*y

" }}}

" Vim Setting Toggles {{{1

nnoremap <silent> ]s :echo "No Spell Checkng"<cr>:setlocal nospell<cr>
nnoremap <silent> ]r :set noreadonly<cr>
nnoremap <silent> ]# :set norelativenumber<cr>
nnoremap <silent> ]m :set nomodifiable<cr>
nnoremap <silent> ]w :set nowrap<cr>

nnoremap <silent> [s :echo "Spell Checking"<cr>:setlocal spell<cr>
nnoremap <silent> [r :set readonly<cr>
nnoremap <silent> [# :set relativenumber<cr>
nnoremap <silent> [m :set modifiable<cr>
nnoremap <silent> [w :set wrap<cr>


" GUI Management Keybinds {{{1
" Toggle full screen mode (should just do nothing in terminal).
nnoremap <silent> <F11> :call helpers#ToggleFullScreen()<CR>


" Close buffers intelligently with Ctrl-C.
nnoremap <silent> <C-c> :call helpers#SmartCloseBuffer()<cr>

" Goto Buffer: Add gb/gB quickbinds.
nnoremap <silent> gB :bnext<cr>
nnoremap <silent> gb :bprevious<cr>
