" If there is more than one argument, definitely dont open Unite.
if argc() > 1 | finish | endif

" If there is one argument and its not the current directory, dont open.
if argc() == 1 && argv(0) != '.' | finish | endif

" Finally, add the autocmd to start recursive search (if applicable).
"   function! s:readMotd()
"       read /Dyn/.dyn_motd.log
"       setl nowrite nomodifiable noswapfile hidden
"       setl buftype=nofile
"       setl bufhidden=hide
"   endfunction


"   autocmd VimEnter * call s:readMotd()
