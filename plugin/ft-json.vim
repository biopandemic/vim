au! BufRead,BufNewFile *.json set filetype=json
au! BufRead,BufNewFile *.tfstate set filetype=json
au! BufRead,BufNewFile *.tfstate.backup set filetype=json

augroup json_autocmd
 autocmd!
 autocmd FileType json set autoindent
 autocmd FileType json set formatoptions=tcq2l
 autocmd FileType json set textwidth=78 shiftwidth=2
 autocmd FileType json set softtabstop=2 tabstop=8
 autocmd FileType json set expandtab
 autocmd FileType json set foldmethod=syntax
 autocmd FileType json set conceallevel=2
augroup END
