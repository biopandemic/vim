" Leader Keybinds
" ---------------
let mapleader=' '

" Rebound temporarily because of habits.
nnoremap <leader>e :NERDTreeToggle<cr>

" Switch Panes
" ------------
nnoremap <silent> <left>  <c-w>h
nnoremap <silent> <down>  <c-w>j
nnoremap <silent> <up>    <c-w>k
nnoremap <silent> <right> <c-w>l

nnoremap <silent> <leader>h <c-w>h
nnoremap <silent> <leader>j <c-w>j
nnoremap <silent> <leader>k <c-w>k
nnoremap <silent> <leader>l <c-w>l


" Submode: Leader 'Open'
" ----------------------
nnoremap <leader>of :NERDTreeToggle<cr>
nnoremap <leader>ot :TagbarToggle<cr>


" Submode: Leader 'Open'
" ----------------------
nnoremap <leader>p :Unite -toggle -buffer-name=source source<cr>


" Submode: Leader 'Vim'
" ---------------------
nnoremap <leader>ve  :tabedit ~/.vim/vimrc<cr>
nnoremap <leader>vs  :source ~/.vim/vimrc<cr>:filetype detect<cr>


" Submode: Leader 'Git'
" ---------------------
nnoremap <leader>gb  :Gblame<cr>
nnoremap <leader>gc  :Gcommit<cr>
nnoremap <leader>gpl :Git pull<cr>
nnoremap <leader>gps :Git push<cr>
nnoremap <leader>gs  :Gstatus<cr>
nnoremap <leader>gw  :Gwrite<cr>


" Submode: Leader 'Buffer'
" ---------------------
nnoremap <leader>bn :bnext<cr>
nnoremap <leader>bp :bprevious<cr>
nnoremap <leader>bd :bdelete<cr>
nnoremap <leader>bD :bdelete!<cr>


" Submode: Leader 'Tab'
" ---------------------
nnoremap <leader>tn :tabNext<cr>
nnoremap <leader>tp :tabPrev<cr>
nnoremap <leader>tc :tabclose<cr>
nnoremap <leader>tC :tabclose!<cr>
