" This file should not be read unless we are in NeoVim.
if !has("nvim") | finish | endif
command! Fm tabedit term://ranger
command! Ps tabedit term://htop

" nmap <silent> <C-Enter> Unite source
command! Term tabnew<cr>:terminal<cr>
command! Vst :vs<cr>:terminal<cr>

" Use <Esc> to exit 'terminal' mode.
tnoremap <Esc><Esc> <C-\><C-n>
