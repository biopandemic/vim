" {{{1 UltiSnips Snippet Completion

" Smartcase: Any capital letters trigger case-sensitive matching.
" Trigger configuration. Do not use <tab> if you use https://github.com/Valloric/YouCompleteMe.
let g:UltiSnipsExpandTrigger="<c-y>"
let g:UltiSnipsJumpForwardTrigger="<tab>"
let g:UltiSnipsJumpBackwardTrigger="<s-tab>"

" If you want :UltiSnipsEdit to split your window.
let g:UltiSnipsEditSplit="vertical"

" {{{1 YouCompleteMe - Autocompletion

" Enable autocompletion inside comments. I frequently want to be able to
" reference variable names in files.
let g:ycm_complete_in_comments = 1

" Do not use comments or strings for populating auto-completion. This is the
" default behavior.
let g:ycm_collect_identifiers_from_comments_and_strings = 0

" Use CTags for auto-completion.  This option is off by default because it
" makes Vim slower if your tags are on a network directory. I rarely have that
" problem.
let g:ycm_collect_identifiers_from_tags_files = 1

" Use Vim syntax files for keyword completion. Not 100% accurate, but nice.
let g:ycm_seed_identifiers_with_syntax = 1

" Some omnicompletion engines do not work well with the YCM cache—in particular,
" they might not produce all possible results for a given prefix. By unsetting
" this option you can ensure that the omnicompletion engine is requeried on every
" keypress. That will ensure all completions will be presented, but might cause
" stuttering and lagginess if the omnifunc is slow.
let g:ycm_cache_omnifunc = 0

" Disable omnicompletion at 10,000kb (10mb), instead of 1mb. Faster machines,
" dude...
let g:ycm_disable_for_files_larger_than_kb = 10000

" Required by Syntastic (YCM sometimes disables it's syntax checkers).
let g:ycm_show_diagnostics_ui = 0

