
" Syntastic - Syntax Checking Bundle

" Use Syntastic's warning counts.
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

" cursor will jump to the first error detected, if any. If
" all issues detected are warnings, the cursor won't jump.
let g:syntastic_auto_jump = 3

let g:syntastic_always_populate_loc_list = 1

" Don't open location list automatically.
let g:syntastic_auto_loc_list = 1

let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 1

" Define Syntastic Symbols
let g:syntastic_error_symbol = '✗'
let g:syntastic_style_error_symbol = '✠'
let g:syntastic_warning_symbol = '∆'
let g:syntastic_style_warning_symbol = '≈'

" Language-specific settings.
let g:syntastic_ruby_checkers=['mri', 'rubocop']

