"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                                 General
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" This is kinda just a common-sense setting that'll blow up if its missing.
set encoding=utf-8
set fileencoding=utf-8

" Define the general settings for various vim related settings.
syntax on

filetype plugin indent on


" Basic Settings
let &shiftwidth=4   "number of spaces when indenting
let &softtabstop=4  "number of spaces per tab in insert mode
let &tabstop=4      "number of spaces per tab for display
set title           "automatically set the terminal title
set autochdir       "automatically change directory to file's parent
set autoindent      "automatically indent to match adjacent lines
set autoread        "Automatically reload files on edit.
set backspace=indent,eol,start  "allow backspacing everything in insert mode
set clipboard=unnamedplus  "Use standard Linux clipboard.
set completeopt=menuone,longest,preview
set cursorline      "Highlight current line by default
set expandtab       "Expands tabs to spaces
set nofoldenable    "Disable vim's folding module.
set foldlevel=0     "Fold everything at startup.
set foldcolumn=0    "Specify foldgutter width in chars (0 to hide).
set foldmethod=syntax   "Use syntax for automatic folding.
set hidden          "Allow buffer switch without saving first.
set incsearch       "Incremental Search, Search as you type
set list            "highlight certain invisble characters
set listchars=tab:│\ ,trail:•,extends:❯,precedes:❮  "invisble chars to highlight
set mouse=a         "Enable mouse integration
set noerrorbells    "Disable audible bell noise (so annoying).
set number          " Show line numbers in the gutter.
set rtp+=~/.fzf     "enable fuzzy finder, cuz its nice.
set relativenumber  "Display relative line number offsets.
set scrolloff=10    "Start scrolling 10 lines before the bottom of pane.
set showbreak=↪\    " Show return characters as this symbol.
set smartcase       "SmartCase for autocompletion.
set smartindent     "smart indenting for additional languages
set smarttab        "use shiftwidth to enter tabs
set t_Co=256        "Set vim to 256 colors.
set t_vb=           "Set visualbell to null.
set nottimeout      "Disable timeout for multi-key binds.
set textwidth=72    "Auto return lines after 72 chars.
set visualbell      "Enable visual bell indicator.
set nowrap          "Disable wrapping long lines of text.
set omnifunc=syntaxcomplete#Complete

set formatoptions=tcqn21r
set showmatch
set matchtime=2
set laststatus=2
set lazyredraw
set noshowmode
set shiftround
set linebreak

" shortmess option helps to avoid all the prompts caused by file messages.
set shortmess+=a    " Abbreviate as much as possible..
set shortmess+=oO   " Allow read/write messages to be skipped.
set shortmess+=s    " Hide 'search hit BOTTOM' messages.
set shortmess+=I    " Hide intro to vim message...
set shortmess+=W    " Silence the 'written' message.
set shortmess-=A    " Always show warnings if theres a swap file.

silent execute '!mkdir -p "' . expand("~/.cache/vim/undos") . '"'
silent execute '!mkdir -p "' . expand("~/.cache/vim/backups") . '"'
silent execute '!mkdir -p "' . expand("~/.cache/vim/swapfiles") . '"'

" Setup the undofiles (if applicable) {{{1
if exists('+undofile')
    set undofile
    set undodir=~/.vim/cache/undos
endif

" Define where to store backup files. {{{1
set backup
set backupdir=~/.vim/cache/backups

" Define where to store cache files. {{{1
set directory=~/.vim/cache/swapfiles
set noswapfile


" Enable coloring on the 80th column. Makes it easier to see returns. {{{1
if exists('+colorcolumn')
  set colorcolumn=80
else
  au BufWinEnter * let w:m2=matchadd('ErrorMsg', '\%>80v.\+', -1)
  highlight OverLength ctermbg=darkgray ctermfg=white guibg=#FFD9D9
  match OverLength /\%>80v.\+/
endif
" Command Mappings           {{{
cnoremap fixhtml :%s#><\([^/]\)#>\r<\l#g<cr>
cnoremap W! SudoWrite %<cr>

" Fugitive Abbreviations
cabbrev Gs Gstatus
cabbrev Gc Gcommit
cabbrev Gps Git push
cabbrev Gpl Git pull

" Global Build Abbreviations (Aliases)
cabbrev Bo Copen
cabbrev Bm Make
cabbrev Bd Dispatch!

cabbrev <silent> Ev tab edit $MYVIMRC
cabbrev <silent> Eft tab edit $MYVIMRC

" Wrap all of the fold binds with an echo foldlevel.
nnoremap zr zr:echo "foldlevel=".&foldlevel<cr>
nnoremap zm zm:echo "foldlevel=".&foldlevel<cr>
nnoremap zR zR:echo "foldlevel=".&foldlevel<cr>
nnoremap zM zM:echo "foldlevel=".&foldlevel<cr>
