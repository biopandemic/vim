" All mouse settings

" Enable mouse in Normal, Visual and Insert modes.
set mouse=nvi

" Enables right click to extend current visual selections.
set mousemodel=extend

set mouseshape=""

" In GUI, do not use mouse's hover to focus buffers.
set nomousefocus

" In GUI, the mouse pointer is hidden when characters are typed.
set mousehide

" Simply ignore all mouse clicks, but allow scrolling.
" for btn in ["RightMouse","2-RightMouse","LeftMouse","2-LeftMouse"]
"     execute 'nnoremap <'. btn .'> <nop>'
"     execute 'inoremap <'. btn .'> <nop>'
"     execute 'vnoremap <'. btn .'> <nop>'
" endfor

" DISABLED: Paste with mouse.
" nnoremap <RightMouse> "+p
" xnoremap <RightMouse> "+p
" nnoremap <RightMouse> <C-r><C-o>+
" cnoremap <RightMouse> <C-r>+
