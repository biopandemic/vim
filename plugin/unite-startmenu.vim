"---------------------------------------------------------------------"
"                      Custom Unite Start Menu
"---------------------------------------------------------------------"
"
" Currently, I bind the menu to "Alt-P", you should be able to change
" it to whatever you like though.
"
"
" Installation: Drop this file into '~/.vim/plugin/' & enjoy!
" Dependencies: Shougo/unite.vim
" Author: Jake Logemann <jake.logemann@gmail.com>
"---------------------------------------------------------------------"
let s:start = {}
let s:start.description = "Start Menu"

inoremap <silent> <M-p> <Esc>:Unite -toggle -buffer-name=start menu:startmenu<cr>
nnoremap <silent> <M-p> :Unite -toggle -buffer-name=start menu:startmenu<cr>
vnoremap <silent> <M-p> :Unite -toggle -buffer-name=start menu:startmenu<cr>

function! UniteBuffer(name)
    return ("Unite -buffer-name=" . a:name . ' ' . a:name)
endfunction

function! OpenURL(url)
    let browser = "firefox"
    let cmd = '!'. browser ." '". a:url ."' 2>/dev/null 1>&2 & disown"
    execute "silent! ". cmd
endfunction

let cc = []
call add(cc, ['▷ Run an executable script ...', UniteBuffer("launcher")])
call add(cc, ['▷ Outline current file ...',     UniteBuffer("outline")])
call add(cc, ['▷ Kill a process...',            UniteBuffer("process")])
call add(cc, ['▷ Search Custom Keybind Mappings', UniteBuffer("mapping")])
call add(cc, ['▷ Search Vim Commands',          UniteBuffer("command") ])
call add(cc, ['▷ Edit UltiSnips ... ',          'UltiSnipsEdit'])
call add(cc, ['▷ Toggle Symbols TagBar',        'TagbarToggle'])
call add(cc, ['▷ Toggle Gundo Tree',            'GundoToggle'])
call add(cc, ['▷ Toggle NERDTree File Browser', 'NERDTreeToggle'])
call add(cc, ['▷ Edit Vimperator Configuration','tabedit ~/.vimperatorrc'])
call add(cc, ['▷ Edit Tmux Configuration',      'tabedit ~/.tmux.conf'])
call add(cc, ['▷ Edit SSH Configuration',       'tabedit ~/.ssh/config'])
call add(cc, ['▷ Edit Bash Configuration',      'tabedit ~/.bashrc'])
call add(cc, ['▷ Toggle search highlighting',   'set hlsearch!'])
call add(cc, ['▷ Toggle line wrapping',         'set wrap!'])
call add(cc, ['▷ Toggle Git Gutter Symbols',    'GitGutterSignsToggle'])
call add(cc, ['▷ Toggle Git Gutter Highlight',  'GitGutterHighlightsToggle'])
call add(cc, ['▷ QuickFix List Directory',      "cexpr system('ls -la') | copen"])
call add(cc, ['▷ Reload VIM Configuration',     'source ~/.vim/vimrc'])
call add(cc, ['▷ Choose a colorscheme ... ',    UniteBuffer('colorscheme')])
call add(cc, ['▷ List all Unite sources ... ',  UniteBuffer('source')])

" Initialize the source menu if it hasn't already been created.
if !exists("g:unite_source_menu_menus")
    let g:unite_source_menu_menus = {}
endif

" Bind our internal 'source' to the created menu variable.
let g:unite_source_menu_menus.startmenu = s:start
let g:unite_source_menu_menus.startmenu.command_candidates = cc
