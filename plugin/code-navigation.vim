" {{{1 NERDTree File Browser Plugin
let NERDTreeAutoCenter = 1
let NERDTreeAutoCenterThreshold = 5

" Never change root nerdtree directory via ':NERDTree /some/new/path'
let NERDTreeChDirMode = 0

" Respect 'wildignore' settings
let NERDTreeRespectWildIgnore = 1

" Put the NERDTree window on the right side, just to be different.
let NERDTreeWinPos = 'left'

" Width of the NERDTree window.
let NERDTreeWinSize = 24

" Automatically parent's children directories if there is only one child.
let NERDTreeCascadeOpenSingleChildDir=1

" Sort capital letters above lower case letters.
let NERDTreeCaseSensitiveSort = 1

" Ignore files ending in: "pyc", "pyo" or "~"
let NERDTreeIgnore=['\.pyc$', '\.pyo$', '\.pyc$', '\~$', '\.git$', '\.vagrant$']

" Store bookmarks within the Vim folder. Keep things clean.
silent execute '!mkdir -p "' . expand("~/.cache/vim") . '"'
let NERDTreeBookmarksFile=expand("~/.cache/vim/NERDTreeBookmarks")

" Do not close NERDTree after a file has been selected.
let NERDTreeQuitOnOpen=0

" Enable bookmarks by default.
let NERDTreeShowBookmarks=0

" Do not show hidden files by default.
let NERDTreeShowHidden = 0

" Disables 'Press ? for help' message.
let NERDTreeMinimalUI=1

" Automatically delete open buffers for renamed/moved files.
let NERDTreeAutoDeleteBuffer=1


" Tagbar - Symbol Navigation Sidebar (Bundle)
" Automatically focus Tagbar when opened.
let g:tagbar_autofocus = 1

" Automatically close tagbar after choosing a symbol.
let g:tagbar_autoclose = 1

" Hide extraneous tagbar help text.
let g:tagbar_compact = 1

" Set smaller indent for tagbar symbols (default: 2).
let g:tagbar_indent = 1

" If this option is set to 1 the Vim window will be expanded by the width of
" the Tagbar window if using a GUI version of Vim. Setting it to 2 will also
" try expanding a terminal, but note that this is not supported by all terms.
let g:tagbar_expand = 0

" Require only a single click to jump to a given symbol.
let g:tagbar_singleclick = 1

" Use a slightly smaller icon for expanding tags.
let g:tagbar_iconchars = ['▸', '▾']

" Automatically open folds when jumping to a tag.
let g:tagbar_autoshowtag = 1

" Moving the cursor in tagbar will show selected tag in preview.
let g:tagbar_autopreview = 0
