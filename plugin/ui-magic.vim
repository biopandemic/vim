"Set custom title for vim. Try it, you'll like it
set title
set titlelen=70
set titlestring=%<%F%=%l/%L-%P

function! TitleStringFocused()
    let &titlestring = "vim:: " . expand("%:p:~:h%:t")
endfunction

function! TitleStringUnfocused()
    let &titlestring = expand("%:t")
endfunction

auto FocusLost              * call TitleStringUnfocused()
auto VimEnter,FocusGained   * call TitleStringFocused()
