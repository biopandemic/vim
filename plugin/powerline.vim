if !exists('g:airline_symbols')
    let g:airline_symbols = {}
endif
" Use unicode symbols for Airline.
let g:airline_left_sep = '»'
let g:airline_right_sep = '«'
let g:airline_symbols.branch = '⎇'
let g:airline_symbols.linenr = '¶'
let g:airline_symbols.paste = 'ρ'
let g:airline_symbols.readonly = '※[RO]※'
let g:airline_symbols.whitespace = 'Ξ'

" enable modified detection
let g:airline_detect_modified=1

" enable paste detection.
let g:airline_detect_paste=1

" enable iminsert detection >
let g:airline_detect_iminsert=0

" Collapse inactive window's status bar, only show file name.
let g:airline_inactive_collapse=1

" Enable auto-population of airline symbols w/ powerline symbols.
let g:airline_powerline_fonts=1

" Exclude preview windows from airline status bar.
let g:airline_exclude_preview = 1

" Customize how Airline display's current mode.
let g:airline_mode_map = {
            \ '__' : '-',
            \ 'n'  : 'N',
            \ 'i'  : 'I',
            \ 'R'  : 'R',
            \ 'c'  : 'C',
            \ 'v'  : 'V',
            \ 'V'  : 'V',
            \ '' : 'V',
            \ 's'  : 'S',
            \ 'S'  : 'S',
            \ '' : 'S',
            \ }
