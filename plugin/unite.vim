" Unite Vim FuzzyFinder Plugin (by Shuguo)

" ------------------------------------------------------------------------"
" Global Configurations
" ------------------------------------------------------------------------"

" Enable Yank history auto completion.
let g:unite_source_history_yank_enable = 1

" Always start in insert mode.
let g:unite_enable_start_insert = 1
let g:unite_enable_short_source_names = 1

" Set number of 'most recently used' (MRU) files to store.
let g:unite_source_file_mru_limit = 100

" Set the update time in milleseconds (default: 500ms).
let g:unite_update_time = 300

" Highlight the line properly, like I do in vim.
let g:unite_cursor_line_highlight = 'CursorLine'

" set prompt line to something cooler.
let g:unite_prompt = ' | ➜ | '


" Use fuzzy matching filter, cuz you know... Sublime does.
call unite#filters#matcher_default#use(['matcher_glob',
            \ 'matcher_hide_current_file',
            \ 'matcher_fuzzy'
            \ ])

" Use rank sorting for everything.
call unite#filters#sorter_default#use([
            \ 'sorter_ftime',
            \ 'sorter_word',
            \ 'sorter_rank'
            \ ])

call unite#custom_default_action('directory,directory_mru', 'cd')

" CtrlP-like settings.
call unite#custom#profile('default', 'context', {
            \   'toggle': 1,
            \   'start_insert': 1,
            \   'auto_resize': 1,
            \   'winheight': 8,
            \   'direction': 'topleft',
            \   'marked_icon': '✓ ',
            \ })



" ------------------------------------------------------------------------"
" Custom Unite Auto-Commands
" ------------------------------------------------------------------------"
" Configuration function for Unite, called each time a buffer is opened.
function! s:SetupUnite()
    " Automatically close Unite when focus is lost.
    autocmd BufLeave,FocusLost <buffer> silent! UniteClose

    imap <buffer> <C-p> <Plug>(unite_all_exit)
    nmap <buffer> <C-p> <Plug>(unite_all_exit)

    " Readline behavior.
    imap <buffer> <c-w> <Plug>(unite_delete_backward_word)

    " Ctrl jk mappings
    imap <buffer> <c-j> <Plug>(unite_insert_leave)
    imap <buffer> <c-k> <Plug>(unite_insert_leave)
    nmap <buffer> <c-j> <Plug>(unite_loop_cursor_down)
    nmap <buffer> <c-k> <Plug>(unite_loop_cursor_up)

    " `` becuase you're lazy, and quit unite
    imap <buffer> `` <Plug>(unite_exit)

    " refresh unite
    nmap <buffer> <C-r> <Plug>(unite_redraw)
    imap <buffer> <C-r> <Plug>(unite_redraw)

    " split control
    noremap <silent><buffer><expr> <C-s> unite#do_action('split')
    noremap <silent><buffer><expr> <C-v> unite#do_action('vsplit')

    " Rotate between modes easier.
    nmap <buffer> <C-Space> <Plug>(unite_rotate_next_source)
    nmap <buffer> <C-[> <Plug>(unite_rotate_previous_source)
endfunction

autocmd FileType unite call s:SetupUnite()
