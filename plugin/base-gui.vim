" Vim GUI Configurations
" ----
" This file should only be run when using gvim.
if !has('gui') | finish | endif

if has("vim_starting")
    " Set default size of vim.
    set lines=45
    set columns=80
endif

" Define Default Font.
set guifont=Inconsolata\ 11

set linespace=2

" Disable tear off menu items
set guioptions-=t

" Remove toolbar icons
set guioptions-=T

" Remove 'footer'
set guioptions-=F

" Remove menubar
set guioptions-=m

" Remove scrollbars
set guioptions-=r
set guioptions-=R
set guioptions-=l
set guioptions-=L
set guioptions-=b

" Add tabs to gui.
set guioptions+=e

" Use console for inline errors instead of a popup.
set guioptions+=c
