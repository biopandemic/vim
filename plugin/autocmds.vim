autocmd GUIEnter * set visualbell t_vb=


" Toggle highlighting the current line when entering/leaving pane
autocmd WinEnter * setlocal cursorline
autocmd WinLeave * setlocal nocursorline


" Strip trailing whitespace automatically.
autocmd BufWritePre * :%s/\s\+$//e

" Default to a text width of 80 chars...
" au BufRead,BufNewFile * setlocal textwidth=80

" File Hooks (autocmd)       {{{
autocmd FocusLost * silent cclose

" Vim Files
" autocmd BufWritePost *vimrc so ~/.vimrc

" Allow vim to browse epub documents.
au BufReadCmd   *.epub      call zip#Browse(expand("<amatch>"))

au BufWritePost *.py call Flake8()

" go back to previous position of cursor if any
autocmd BufReadPost *
            \ if line("'\"") > 0 && line("'\"") <= line("$") |
            \  exe 'normal! g`"zvzz' |
            \ endif

" coffeescript
autocmd BufWritePost,FileWritePost *.coffee silent !coffee -c <afile>

" Python Autocommands
autocmd BufEnter *.py silent let g:pymode=1
autocmd BufLeave *.py silent let g:pymode=0

autocmd BufRead,BufNewFile *.Dockerfile setfiletype Dockerfile
autocmd BufRead,BufNewFile *.html setlocal textwidth=78
autocmd BufRead,BufNewFile *.py   setlocal textwidth=78
autocmd BufRead,BufNewFile *.md   setlocal textwidth=78
" }}}
