#!/usr/bin/env zsh
# -----------------------------------------------------------------------------
# @CURSOR@
#
#
# Author: Jake Logemann <jake.logemann@gmail.com>
# -----------------------------------------------------------------------------


function _init(){
    echo "Args are: ${@:-No args given}"
}; _init $@
