if has('vim_starting')
    " Skip initialization on vim-tiny/vim-small.
    if !1 | finish | endif
endif

" autocmd VimEnter * :PlugInstall

call plug#begin('~/.vim/plugged')

" Color Schemes {{{1
Plug 'nanotech/jellybeans.vim'
Plug 'blerins/flattown'
Plug 'whatyouhide/vim-gotham'

" Tim Pope's Plugins {{{1
Plug 'tpope/vim-vinegar'
Plug 'tpope/vim-vinegar'
Plug 'tpope/vim-sleuth'
Plug 'tpope/vim-speeddating'
Plug 'tpope/vim-unimpaired'
Plug 'tpope/vim-markdown'

" Better Customization {{{1
Plug 'amoffat/snake', { 'on': [] }
Plug 'benmills/vimux'


" Essential enhancements   {{{1
Plug 'reedes/vim-lexical'       "Spellcheck/Thesaurus/Dictionary
Plug 'reedes/vim-wordy'         "Minimalistic writing analysis tool.
Plug 'noahfrederick/vim-skeleton'  "Default filetype-specific templates.
Plug 'osyo-manga/vim-over'     "Overlay find/replace over text.
Plug 'Glench/Vim-Jinja2-Syntax'
Plug 'bruno-/vim-man'          "Easier Man page browsing.
Plug 'xolox/vim-session'       "SaveSession / OpenSession
Plug 'vimwiki/vimwiki'         "Knowledgebase / Wiki.
Plug 'rking/ag.vim'            "Great replacement for Ack code search.
Plug 'sotte/presenting.vim'    "Easy VIM Presentations.
Plug 'Lokaltog/vim-easymotion', { 'on': [] } "Lazy jumps similar to Vimperator nav.
Plug 'Shougo/unite.vim'        "CtrlP-ish Fuzzy Finder.
Plug 'Shougo/unite-outline'    "Merges Tagbar w/ Unite.vim

"AutoComplete (python-based).
Plug 'Valloric/YouCompleteMe', { 'do': './install.sh' }

"CTags 'Symbols' Sidebar
Plug 'majutsushi/tagbar' , { 'on': 'TagBarToggle' }
Plug 'scrooloose/NERDTree', { 'on': 'NERDTreeToggle' }  "File Browser.
Plug 'scrooloose/syntastic'    "Syntax Checker

"AutoComplete Snippets
Plug 'sirver/ultisnips' | Plug 'honza/vim-snippets'

Plug 'sjl/gundo.vim', { 'on': 'GundoToggle' } "Tree-based Undo
Plug 'tpope/vim-commentary'    "Adds 'toggle comment' keybind.
Plug 'tpope/vim-dispatch'      "Allows calling build functions in bg.
Plug 'tpope/vim-eunuch'        "SudoWrite, need I say more?
Plug 'tpope/vim-fugitive'      "Super-powered GIT! Blame, Status, etc.
Plug 'tpope/vim-git'           "Basic Vim GIT Support.
Plug 'gregsexton/gitv'         "GIT GUI in Vim (requires fugitive).
Plug 'tpope/vim-jdaddy'        "JSON navigation motions (eg: gqaj)
Plug 'tpope/vim-repeat'        "Allows using '.' to repeat more actions.
Plug 'tpope/vim-sensible'      "Sensible Defaults for vim.
Plug 'tpope/vim-surround'      "Quick manipulation of quotes/parens/etc.
Plug 'vim-misc'                "Required by 'tpope/vim-dispatch'
Plug 'wellle/targets.vim'      "Adds additional motions for quicker nav.


" VIM Aesthetics {{{1
" Turns Python reserved words to symbols.
Plug 'junegunn/goyo.vim'            "Distraction-Free WriteRoom Style Editing
Plug 'justincampbell/vim-eighties'  "Automatic buffer resizing.
Plug 'ehamberg/vim-cute-python', { 'for': 'python' }
Plug 'airblade/vim-gitgutter'      "Highlights changes in vim's gutter.
Plug 'bling/vim-airline'           "Powerline Statusbar Alternative.
Plug 'godlygeek/tabular'           "Makes it easier to align tables.
Plug 'vim-scripts/liquidfold.vim'  "Adds Syntax Folding for Django Tmpls

" Code Validation / Cleanup {{{1
Plug 'pangloss/vim-javascript', { 'for': 'javascript' }
Plug 'othree/javascript-libraries-syntax.vim', { 'for': 'javascript' }
Plug 'othree/yajs.vim', { 'for': 'javascript' }  "YetAnotherJS Vim
Plug 'nvie/vim-flake8', { 'for': 'python' }  "Python PEP-8 Flake support.
Plug 'Raimondi/delimitMate' "Automatically close brackets, etc.

" Ruby/Chef/Automation {{{1
Plug 'vim-ruby/vim-ruby', { 'for': 'ruby' }
Plug 'vadv/vim-chef'
Plug 'Shougo/vimproc.vim', { 'do': 'make' }

" Add some additional syntaxes  {{{1
Plug 'plasticboy/vim-markdown', { 'for': 'mkd' }
Plug 'rust-lang/rust.vim', { 'for': 'rust' }  "Rust Language
Plug 'markcornick/vim-terraform' " Terraform Syntax Highlighting
Plug 'chase/vim-ansible-yaml'   "Autodetects Ansible Syntax (dir struct).
Plug 'fatih/vim-go', { 'for': 'go' }
Plug 'klen/python-mode', { 'for': 'python' }
Plug 'nginx.vim'                "NGINX Config Syntax.
Plug 'Matt-Deacalion/vim-systemd-syntax', { 'for': 'systemd' }
Plug 'ekalinin/Dockerfile.vim', { 'for': 'Dockerfile' }
Plug 'groenewege/vim-less'      "LESS CSS Pre-Processor Syntax.
Plug 'digitaltoad/vim-jade'     "Jade Template Snytax Highlighting.
Plug 'kchmck/vim-coffee-script' "Coffeescript Syntax Highlighting.
Plug 'plasticboy/vim-markdown'  "Markdown Syntax Highlighting.
Plug 'othree/html5.vim', { 'for': 'html' }
Plug 'mxw/vim-jsx'              "React.js Syntax Highlighting.
Plug 'elzr/vim-json', { 'for': 'json' }
Plug 'jamessan/vim-gnupg'  "Enables editing of GPG-encrypted files on-the-fly.

" NeoVim Specific Plugins {{{1
if has("nvim")
    Plug 'benekastah/neomake'
endif

call plug#end()
